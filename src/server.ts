import http from "http";
import path from "path";
import express, { Request, Response } from "express";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import { Server } from "socket.io";

import { authMiddleware, authController } from "./controllers/auth";
import {
  getOrders,
  getOrdersByQuery,
  getOrdersByProductId,
  uploadOrders,
} from "./controllers/orders";

import { Order } from "./types";

const ORDER_EVENT = "order";
const STATIC_PATH = path.join(__dirname, "..", "./static");

const app = express();
const server = http.createServer(app);
const io = new Server(server);

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req: Request, res: Response) => {
  res.sendFile(path.join(STATIC_PATH, "index.html"));
});
app.get(/\/(index|login)\.html/, express.static(STATIC_PATH));
app.get(/\/*\.html/, authMiddleware, express.static(STATIC_PATH));

app.post("/auth", authController);
app.get("/show", authMiddleware, getOrders);
app.get("/search", authMiddleware, getOrdersByQuery);
app.post("/upload", authMiddleware, uploadOrders);

io.sockets.on("connection", (socket) => {
  socket.on("subscribe", (productList: string) => {
    const parsedProductList: number[] = JSON.parse(productList);
    const productIds = parsedProductList.map((id) => String(id));

    socket.join(productIds);

    const orders = getOrdersByProductId(productIds);

    orders.map((order) => socket.emit(ORDER_EVENT, JSON.stringify(order)));
  });
});

export function sendUploadedOrder(order: Order) {
  const productId = String(order.productId);

  io.sockets.in(productId).emit(ORDER_EVENT, JSON.stringify(order));
}

export default server;
