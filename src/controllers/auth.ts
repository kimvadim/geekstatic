import { Request, Response, NextFunction } from "express";

import { SERVER_ERROR } from "../constants";

import { default as users } from "../../users.json";

const REDIRECT_PATH = "/login.html";

const COOKIE_NAME = "SHIPPING_FACILITY";
const COOKIE_OPTIONS = {
  httpOnly: true,
  maxAge: 24 * 60 * 60 * 1000,
};

export function authMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  try {
    if (!req.cookies[COOKIE_NAME]) res.redirect(REDIRECT_PATH);
    else next();
  } catch (error) {
    console.error(error);

    res.status(500).send({ message: SERVER_ERROR });
  }
}

export function authController(req: Request, res: Response) {
  try {
    const { username, password } = req.body;

    const user = authenticateUser(username, password);

    if (!user) res.redirect(REDIRECT_PATH);
    else {
      res.cookie(COOKIE_NAME, user.username, COOKIE_OPTIONS);

      res.redirect("/home.html");
    }
  } catch (error) {
    console.error(error);

    res.status(500).send({ message: SERVER_ERROR });
  }
}

function authenticateUser(username: string, password: string) {
  const user = users.find(
    (u) => u.username === username && u.password === password
  );

  return user;
}
