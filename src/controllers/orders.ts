import path from "path";
import { writeFile } from "fs/promises";
import { Request, Response } from "express";

import { sendUploadedOrder } from "../server";
import { SERVER_ERROR } from "../constants";

import { default as rawOrders } from "../../orders.json";
import { default as storeOrders } from "../store.json";
import { default as products } from "../../products.json";
import { default as customers } from "../../customers.json";

import { Order, Search, UploadOrder, UploadOrderViaParameters } from "../types";

let orders: Order[] = storeOrders.length
  ? storeOrders
  : initializeOrders(rawOrders);

export function getOrders(req: Request, res: Response) {
  try {
    res.status(200).send(orders);
  } catch (error) {
    console.error(error);

    res.status(500).send({ message: SERVER_ERROR });
  }
}

export function getOrdersByQuery(req: Request, res: Response) {
  try {
    const ordersByQuery = searchOrders(req.query);

    res.status(200).send(JSON.stringify(ordersByQuery));
  } catch (error) {
    console.error(error);

    res.status(500).send({ message: SERVER_ERROR });
  }
}

export function getOrdersByProductId(productIds: string[]): Order[] {
  return orders.filter((o) => productIds.includes(String(o.productId)));
}

export async function uploadOrders(req: Request, res: Response) {
  try {
    let rawOrders = req.body;

    if (!req.is("application/json"))
      rawOrders = formatUploadOrderViaParameters(rawOrders);

    const newOrders = formatOrders(rawOrders);

    newOrders.map(processOrder);

    await saveOrdersToFile(orders);

    res.status(200).send({ data: newOrders });
  } catch (error) {
    console.error(error);

    res.status(500).send({ message: SERVER_ERROR });
  }
}

function searchOrders(search: Search): Array<Order> {
  return orders.filter((o: Order) => {
    return (
      ((search.buyer && search.buyer === o.buyer) || !search.buyer) &&
      ((search.productId && search.productId === String(o.productId)) ||
        !search.productId) &&
      ((search.shippingTarget &&
        Number(search.shippingTarget) <= o.shippingTarget) ||
        !search.shippingTarget)
    );
  });
}

function initializeOrders(uploadOrders: UploadOrder[]): Order[] {
  const formattedOrders = uploadOrders.map(formatOrders);

  const initialOrders = formattedOrders.flat();

  saveOrdersToFile(initialOrders);

  return initialOrders;
}

function formatOrders(uploadOrder: UploadOrder): Order[] {
  return uploadOrder.items.map((orderItem) => {
    const { item, quantity } = orderItem;
    const [hours, minutes] = uploadOrder.shippingTime.split(":");

    const product = products.find((p) => p.name === item);
    const productId = product?.productId || 0;

    const customer = customers.find((c) => c.name === uploadOrder.buyer);
    const shippingAddress = customer?.address || "";

    const shippingTarget = new Date(uploadOrder.shippingDate).setHours(
      Number(hours),
      Number(minutes)
    );

    return {
      buyer: uploadOrder.buyer,
      productId,
      quantity: Number(quantity),
      shippingAddress,
      shippingTarget,
    };
  });
}

function formatUploadOrderViaParameters(
  uploadOrder: UploadOrderViaParameters
): UploadOrder {
  const { buyer, orderDate, orderTime, shippingDate, shippingTime, saleRoute } =
    uploadOrder;

  const items = uploadOrder.item.map((item, i) => {
    const quantity = Number(uploadOrder.quantity[i]);

    return {
      item,
      quantity,
    };
  });

  return {
    buyer,
    orderDate,
    orderTime,
    shippingDate,
    shippingTime,
    saleRoute,
    items,
  };
}

function processOrder(order: Order) {
  orders.push(order);

  sendUploadedOrder(order);
}

async function saveOrdersToFile(orders: Order[]) {
  const filePath = path.join(__dirname, "..", "store.json");
  const data = JSON.stringify(orders);

  await writeFile(filePath, data);
}
