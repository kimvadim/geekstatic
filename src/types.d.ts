export type Order = {
  buyer: string;
  productId: number;
  quantity: number;
  shippingAddress: string;
  shippingTarget: number;
};

interface UploadOrderItem {
  item: string;
  quantity: number | null;
}

export interface UploadOrder {
  buyer: string;
  items: UploadOrderItem[];
  orderDate: string;
  orderTime: string;
  shippingDate: string;
  shippingTime: string;
  saleRoute: string;
}

export interface UploadOrderViaParameters {
  buyer: string;
  item: string[];
  quantity: string[];
  orderDate: string;
  orderTime: string;
  shippingDate: string;
  shippingTime: string;
  saleRoute: string;
  Submit: string;
}

export interface Search {
  buyer?: string;
  productId?: string;
  shippingTarget?: string;
}
